// Compiled by ClojureScript 1.9.473 {}
goog.provide('adzerk.boot_reload.reload');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('goog.Uri');
goog.require('goog.async.DeferredList');
goog.require('goog.net.jsloader');
adzerk.boot_reload.reload.page_uri = (function adzerk$boot_reload$reload$page_uri(){
return (new goog.Uri(window.location.href));
});
adzerk.boot_reload.reload.ends_with_QMARK_ = (function adzerk$boot_reload$reload$ends_with_QMARK_(s,pat){
return cljs.core._EQ_.call(null,pat,cljs.core.subs.call(null,s,(cljs.core.count.call(null,s) - cljs.core.count.call(null,pat))));
});
adzerk.boot_reload.reload.reload_page_BANG_ = (function adzerk$boot_reload$reload$reload_page_BANG_(){
return window.location.reload();
});
adzerk.boot_reload.reload.normalize_href_or_uri = (function adzerk$boot_reload$reload$normalize_href_or_uri(href_or_uri){
var uri = (new goog.Uri(href_or_uri));
return adzerk.boot_reload.reload.page_uri.call(null).resolve(uri).getPath();
});
/**
 * Produce the changed goog.Uri iff the (relative) path is different
 *   compared to the content of changed (a string). Return nil otherwise.
 */
adzerk.boot_reload.reload.changed_uri = (function adzerk$boot_reload$reload$changed_uri(href_or_uri,changed){
if(cljs.core.truth_(href_or_uri)){
var path = adzerk.boot_reload.reload.normalize_href_or_uri.call(null,href_or_uri);
var temp__4657__auto__ = cljs.core.first.call(null,cljs.core.filter.call(null,((function (path){
return (function (p1__8329_SHARP_){
return adzerk.boot_reload.reload.ends_with_QMARK_.call(null,adzerk.boot_reload.reload.normalize_href_or_uri.call(null,p1__8329_SHARP_),path);
});})(path))
,changed));
if(cljs.core.truth_(temp__4657__auto__)){
var changed__$1 = temp__4657__auto__;
return goog.Uri.parse(changed__$1);
} else {
return null;
}
} else {
return null;
}
});
adzerk.boot_reload.reload.reload_css = (function adzerk$boot_reload$reload$reload_css(changed){
var sheets = document.styleSheets;
var seq__8334 = cljs.core.seq.call(null,cljs.core.range.call(null,(0),sheets.length));
var chunk__8335 = null;
var count__8336 = (0);
var i__8337 = (0);
while(true){
if((i__8337 < count__8336)){
var s = cljs.core._nth.call(null,chunk__8335,i__8337);
var temp__4657__auto___8338 = (sheets[s]);
if(cljs.core.truth_(temp__4657__auto___8338)){
var sheet_8339 = temp__4657__auto___8338;
var temp__4657__auto___8340__$1 = adzerk.boot_reload.reload.changed_uri.call(null,sheet_8339.href,changed);
if(cljs.core.truth_(temp__4657__auto___8340__$1)){
var href_uri_8341 = temp__4657__auto___8340__$1;
sheet_8339.ownerNode.href = href_uri_8341.makeUnique().toString();
} else {
}
} else {
}

var G__8342 = seq__8334;
var G__8343 = chunk__8335;
var G__8344 = count__8336;
var G__8345 = (i__8337 + (1));
seq__8334 = G__8342;
chunk__8335 = G__8343;
count__8336 = G__8344;
i__8337 = G__8345;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__8334);
if(temp__4657__auto__){
var seq__8334__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__8334__$1)){
var c__7932__auto__ = cljs.core.chunk_first.call(null,seq__8334__$1);
var G__8346 = cljs.core.chunk_rest.call(null,seq__8334__$1);
var G__8347 = c__7932__auto__;
var G__8348 = cljs.core.count.call(null,c__7932__auto__);
var G__8349 = (0);
seq__8334 = G__8346;
chunk__8335 = G__8347;
count__8336 = G__8348;
i__8337 = G__8349;
continue;
} else {
var s = cljs.core.first.call(null,seq__8334__$1);
var temp__4657__auto___8350__$1 = (sheets[s]);
if(cljs.core.truth_(temp__4657__auto___8350__$1)){
var sheet_8351 = temp__4657__auto___8350__$1;
var temp__4657__auto___8352__$2 = adzerk.boot_reload.reload.changed_uri.call(null,sheet_8351.href,changed);
if(cljs.core.truth_(temp__4657__auto___8352__$2)){
var href_uri_8353 = temp__4657__auto___8352__$2;
sheet_8351.ownerNode.href = href_uri_8353.makeUnique().toString();
} else {
}
} else {
}

var G__8354 = cljs.core.next.call(null,seq__8334__$1);
var G__8355 = null;
var G__8356 = (0);
var G__8357 = (0);
seq__8334 = G__8354;
chunk__8335 = G__8355;
count__8336 = G__8356;
i__8337 = G__8357;
continue;
}
} else {
return null;
}
}
break;
}
});
adzerk.boot_reload.reload.reload_img = (function adzerk$boot_reload$reload$reload_img(changed){
var images = document.images;
var seq__8362 = cljs.core.seq.call(null,cljs.core.range.call(null,(0),images.length));
var chunk__8363 = null;
var count__8364 = (0);
var i__8365 = (0);
while(true){
if((i__8365 < count__8364)){
var s = cljs.core._nth.call(null,chunk__8363,i__8365);
var temp__4657__auto___8366 = (images[s]);
if(cljs.core.truth_(temp__4657__auto___8366)){
var image_8367 = temp__4657__auto___8366;
var temp__4657__auto___8368__$1 = adzerk.boot_reload.reload.changed_uri.call(null,image_8367.src,changed);
if(cljs.core.truth_(temp__4657__auto___8368__$1)){
var href_uri_8369 = temp__4657__auto___8368__$1;
image_8367.src = href_uri_8369.makeUnique().toString();
} else {
}
} else {
}

var G__8370 = seq__8362;
var G__8371 = chunk__8363;
var G__8372 = count__8364;
var G__8373 = (i__8365 + (1));
seq__8362 = G__8370;
chunk__8363 = G__8371;
count__8364 = G__8372;
i__8365 = G__8373;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__8362);
if(temp__4657__auto__){
var seq__8362__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__8362__$1)){
var c__7932__auto__ = cljs.core.chunk_first.call(null,seq__8362__$1);
var G__8374 = cljs.core.chunk_rest.call(null,seq__8362__$1);
var G__8375 = c__7932__auto__;
var G__8376 = cljs.core.count.call(null,c__7932__auto__);
var G__8377 = (0);
seq__8362 = G__8374;
chunk__8363 = G__8375;
count__8364 = G__8376;
i__8365 = G__8377;
continue;
} else {
var s = cljs.core.first.call(null,seq__8362__$1);
var temp__4657__auto___8378__$1 = (images[s]);
if(cljs.core.truth_(temp__4657__auto___8378__$1)){
var image_8379 = temp__4657__auto___8378__$1;
var temp__4657__auto___8380__$2 = adzerk.boot_reload.reload.changed_uri.call(null,image_8379.src,changed);
if(cljs.core.truth_(temp__4657__auto___8380__$2)){
var href_uri_8381 = temp__4657__auto___8380__$2;
image_8379.src = href_uri_8381.makeUnique().toString();
} else {
}
} else {
}

var G__8382 = cljs.core.next.call(null,seq__8362__$1);
var G__8383 = null;
var G__8384 = (0);
var G__8385 = (0);
seq__8362 = G__8382;
chunk__8363 = G__8383;
count__8364 = G__8384;
i__8365 = G__8385;
continue;
}
} else {
return null;
}
}
break;
}
});
adzerk.boot_reload.reload.reload_js = (function adzerk$boot_reload$reload$reload_js(changed,p__8388){
var map__8391 = p__8388;
var map__8391__$1 = ((((!((map__8391 == null)))?((((map__8391.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__8391.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__8391):map__8391);
var on_jsload = cljs.core.get.call(null,map__8391__$1,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),cljs.core.identity);
var js_files = cljs.core.filter.call(null,((function (map__8391,map__8391__$1,on_jsload){
return (function (p1__8386_SHARP_){
return adzerk.boot_reload.reload.ends_with_QMARK_.call(null,p1__8386_SHARP_,".js");
});})(map__8391,map__8391__$1,on_jsload))
,changed);
if(cljs.core.seq.call(null,js_files)){
goog.net.jsloader.loadMany(cljs.core.clj__GT_js.call(null,cljs.core.map.call(null,((function (js_files,map__8391,map__8391__$1,on_jsload){
return (function (p1__8387_SHARP_){
return goog.Uri.parse(p1__8387_SHARP_).makeUnique();
});})(js_files,map__8391,map__8391__$1,on_jsload))
,js_files)),({"cleanupWhenDone": true})).addCallbacks(((function (js_files,map__8391,map__8391__$1,on_jsload){
return (function() { 
var G__8393__delegate = function (_){
return on_jsload.call(null);
};
var G__8393 = function (var_args){
var _ = null;
if (arguments.length > 0) {
var G__8394__i = 0, G__8394__a = new Array(arguments.length -  0);
while (G__8394__i < G__8394__a.length) {G__8394__a[G__8394__i] = arguments[G__8394__i + 0]; ++G__8394__i;}
  _ = new cljs.core.IndexedSeq(G__8394__a,0);
} 
return G__8393__delegate.call(this,_);};
G__8393.cljs$lang$maxFixedArity = 0;
G__8393.cljs$lang$applyTo = (function (arglist__8395){
var _ = cljs.core.seq(arglist__8395);
return G__8393__delegate(_);
});
G__8393.cljs$core$IFn$_invoke$arity$variadic = G__8393__delegate;
return G__8393;
})()
;})(js_files,map__8391,map__8391__$1,on_jsload))
,((function (js_files,map__8391,map__8391__$1,on_jsload){
return (function (e){
return console.error("Load failed:",e.message);
});})(js_files,map__8391,map__8391__$1,on_jsload))
);

if(cljs.core.truth_((window["jQuery"]))){
return jQuery(document).trigger("page-load");
} else {
return null;
}
} else {
return null;
}
});
adzerk.boot_reload.reload.reload_html = (function adzerk$boot_reload$reload$reload_html(changed){
var page_path = adzerk.boot_reload.reload.page_uri.call(null).getPath();
var html_path = (cljs.core.truth_(adzerk.boot_reload.reload.ends_with_QMARK_.call(null,page_path,"/"))?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(page_path),cljs.core.str.cljs$core$IFn$_invoke$arity$1("index.html")].join(''):page_path);
if(cljs.core.truth_(adzerk.boot_reload.reload.changed_uri.call(null,html_path,changed))){
return adzerk.boot_reload.reload.reload_page_BANG_.call(null);
} else {
return null;
}
});
adzerk.boot_reload.reload.group_log = (function adzerk$boot_reload$reload$group_log(title,things_to_log){
console.groupCollapsed(title);

var seq__8400_8404 = cljs.core.seq.call(null,things_to_log);
var chunk__8401_8405 = null;
var count__8402_8406 = (0);
var i__8403_8407 = (0);
while(true){
if((i__8403_8407 < count__8402_8406)){
var t_8408 = cljs.core._nth.call(null,chunk__8401_8405,i__8403_8407);
console.log(t_8408);

var G__8409 = seq__8400_8404;
var G__8410 = chunk__8401_8405;
var G__8411 = count__8402_8406;
var G__8412 = (i__8403_8407 + (1));
seq__8400_8404 = G__8409;
chunk__8401_8405 = G__8410;
count__8402_8406 = G__8411;
i__8403_8407 = G__8412;
continue;
} else {
var temp__4657__auto___8413 = cljs.core.seq.call(null,seq__8400_8404);
if(temp__4657__auto___8413){
var seq__8400_8414__$1 = temp__4657__auto___8413;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__8400_8414__$1)){
var c__7932__auto___8415 = cljs.core.chunk_first.call(null,seq__8400_8414__$1);
var G__8416 = cljs.core.chunk_rest.call(null,seq__8400_8414__$1);
var G__8417 = c__7932__auto___8415;
var G__8418 = cljs.core.count.call(null,c__7932__auto___8415);
var G__8419 = (0);
seq__8400_8404 = G__8416;
chunk__8401_8405 = G__8417;
count__8402_8406 = G__8418;
i__8403_8407 = G__8419;
continue;
} else {
var t_8420 = cljs.core.first.call(null,seq__8400_8414__$1);
console.log(t_8420);

var G__8421 = cljs.core.next.call(null,seq__8400_8414__$1);
var G__8422 = null;
var G__8423 = (0);
var G__8424 = (0);
seq__8400_8404 = G__8421;
chunk__8401_8405 = G__8422;
count__8402_8406 = G__8423;
i__8403_8407 = G__8424;
continue;
}
} else {
}
}
break;
}

return console.groupEnd();
});
/**
 * Perform heuristics to check if a non-shimmed DOM is available
 */
adzerk.boot_reload.reload.has_dom_QMARK_ = (function adzerk$boot_reload$reload$has_dom_QMARK_(){
return (typeof window !== 'undefined') && (typeof window.document !== 'undefined') && (typeof window.document.documentURI !== 'undefined');
});
adzerk.boot_reload.reload.reload = (function adzerk$boot_reload$reload$reload(changed,opts){
var changed_STAR_ = cljs.core.map.call(null,(function (p1__8425_SHARP_){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"asset-host","asset-host",-899289050).cljs$core$IFn$_invoke$arity$1(opts)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__8425_SHARP_)].join('');
}),cljs.core.map.call(null,(function (p__8431){
var map__8432 = p__8431;
var map__8432__$1 = ((((!((map__8432 == null)))?((((map__8432.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__8432.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__8432):map__8432);
var canonical_path = cljs.core.get.call(null,map__8432__$1,new cljs.core.Keyword(null,"canonical-path","canonical-path",-1891747854));
var web_path = cljs.core.get.call(null,map__8432__$1,new cljs.core.Keyword(null,"web-path","web-path",594590673));
if(cljs.core._EQ_.call(null,"file:",(function (){var G__8434 = window;
var G__8434__$1 = (((G__8434 == null))?null:G__8434.location);
if((G__8434__$1 == null)){
return null;
} else {
return G__8434__$1.protocol;
}
})())){
return canonical_path;
} else {
return web_path;
}
}),changed));
adzerk.boot_reload.reload.group_log.call(null,"Reload",changed_STAR_);

adzerk.boot_reload.reload.reload_js.call(null,changed_STAR_,opts);

if(cljs.core.truth_(adzerk.boot_reload.reload.has_dom_QMARK_.call(null))){
var G__8435 = changed_STAR_;
adzerk.boot_reload.reload.reload_html.call(null,G__8435);

adzerk.boot_reload.reload.reload_css.call(null,G__8435);

adzerk.boot_reload.reload.reload_img.call(null,G__8435);

return G__8435;
} else {
return null;
}
});

//# sourceMappingURL=reload.js.map